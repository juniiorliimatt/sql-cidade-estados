create sequence role_sequence start 1 increment 1;
create sequence user_sequence start 1 increment 1;

create table roles ( 
  id int8 not null, name varchar(255), primary key (id)
);

create table users (
  id int8 not null, password varchar(255) not null, username varchar(255) not null, primary key (id)
);

create table users_roles (
    user_id int8 not null, roles_id int8 not null
);

alter table if exists roles 
add constraint uk_role_name unique (name);

alter table if exists users 
add constraint uk_user_username unique (username);

alter table if exists users_roles 
add constraint fk_role_id
foreign key (roles_id) 
references roles;

alter table if exists users_roles 
add constraint fk_user_id
foreign key (user_id) 
references users;

insert into
roles (id, name) 
values (1, 'ROLE_ADMIN');

insert into
users (id, password, username) 
values (1, '$2a$10$RShbUXNJrqZRXn77BMQ2WOuG.rlcqKLhtMGujR5j9XZ9jwTXVaUYO', 'juniiorliimatt@gmail.com')

insert into
users_roles (user_id, roles_id) 
values (1, 1)